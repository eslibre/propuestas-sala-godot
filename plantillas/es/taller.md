---
layout: 2021/post
section: proposals
category: workshops
author: NOMBRE
title: TÍTULO-TALLER
---

## Descripción

[DESCRIPCIÓN: Descripción algo más extensa sobre la temática y el contenido de la propuesta.]

## Objetivos a cubrir en el taller

[OBJETIVOS A CUBRIR EN EL TALLER: ¿Qué te gustaría que las personas que asistan al taller salgan conociendo en mayor o menor medida?]

-   Web del proyecto: [URL]

## Público objetivo

[PÚBLICO OBJETIVO: ¿A quién va dirigida?]

## Ponente(s)

[PONENTE: Necesitaríamos algo de información general sobre la persona o personas que van a llevar a cabo la propuesta: intereses personales, experiencia en la materia, si has realizado actividades similares...]

### Contacto(s)

-   Nombre: [NOMBRE]
-   Email: [EMAIL]
-   Web personal: [URL]
-   Mastodon (u otras redes sociales libres): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio o GitHub (u otros sitios de código colaborativo): [URL]

## Prerrequisitos para los asistentes

[PRERREQUISITOS PARA LOS ASISTENTES: Descripción de conocimientos mínimos recomendados, así como hardware/software del que deberían disponer las personas asistentes a la actividad.]

## Prerrequisitos para la organización

[PRERREQUISITOS PARA LA ORGANIZACIÓN: Descripción de elementos que sería ideal que la organización pudiera proveer para la correcta realización de la actividad (ejemplo: disposición de máquinas virtuales conun determinado software instalado para las personas que asistan a la actividad). La organización no garantiza que se puedan satisfacer las necesidades de las actividades propuestos.]

## Preferencia horaria

-   Duración: [Indicar la duración de la propuesta. Por ejemplo, desde dos horas hasta un día entero.]
-   Día: [Indicar si se prefiere que sea el primer día o el segundo.]

## Comentarios

[COMENTARIOS: Cualquier otro comentario relevante para la organización.]

## Preferencias de privacidad

(Si quieres que tu información de contacto sea anónima, mándamos las propuesta mediante los formularios de la web: <https://propuestas-godot.eslib.re/2021/propuestas/talleres/>)

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información del taller.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información del taller.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) de esLibre y solicitar a las personas asistentes su cumplimiento.
-   [x]  Acepto coordinarme con la organización de esLibre para la realización del taller.
-   [x]  Confirmo que al menos una persona de entre las que proponen el taller estará conectada el día programado para impartirlo.
