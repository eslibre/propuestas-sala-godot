---
layout: 2021/post
section: proposals
category: talks
author: NOMBRE
title: TITULO-CHARLA
---

[RESUMEN: Pequeña introducción y motivación de la propuesta.]

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

[DESCRIPCIÓN: Descripción algo más extensa sobre la temática y el contenido de la propuesta.]

-   Web del proyecto: [URL]

## Público objetivo

[PÚBLICO OBJETIVO: ¿A quién va dirigida?]

## Ponente(s)

[PONENTE(S): Necesitaríamos algo de información general sobre la persona o personas que van a llevar a cabo la propuesta: intereses personales, experiencia en la materia, si has realizado actividades similares...]

### Contacto(s)

-   Nombre: [NOMBRE]
-   Email: [EMAIL]
-   Web personal: [URL]
-   Mastodon (u otras redes sociales libres): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio o GitHub (u otros sitios de código colaborativo): [URL]

## Comentarios

[COMENTARIOS: Cualquier otro comentario relevante para la organización.]

## Preferencias de privacidad

(Si quieres que tu información de contacto sea anónima, mándamos las propuesta mediante los formularios de la web: <https://propuestas-godot.eslib.re/2021/propuestas/charlas/>)

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) de esLibre y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
