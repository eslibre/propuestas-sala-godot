---
layout: 2021/post
category: talks
author: Juan José Ramos Muñoz
title: M.A.T.I.L.D.A., un videojuego para la docencia de redes de computadores
---

## {{ page.author }} - {{ page.title }}

 M.A.T.I.L.D.A. (MODULAR TELEMATIC TOPICS RELATED DEVELOPMENT PLATFORM) es un proyecto basado en Godot Engine, que se diseñó para motivar a estudiantes que utilizaran Java para diseño e implementación de protocolos.
 La plataforma permite comunicar un programa en Java con varios minijuegos desarrollados en Godot.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

 M.A.T.I.L.D.A. (MODULAR TELEMATIC TOPICS RELATED DEVELOPMENT PLATFORM) es un proyecto basado en Godot Engine, que se diseñó para motivar a estudiantes que utilizaran Java para diseño e implementación de protocolos.
 La plataforma permite comunicar un programa en Java con varios minijuegos desarrollados en Godot.

-   Web del proyecto: <https://matildagame.github.io/>

## Público objetivo

Docentes. Usuarios de Godot. Estudiantes de programación de aplicaciones en red.

## Ponente(s)

Juan José Ramos Muñoz es profesor del departamento de Teoría de la Señal, Telemática y Comunicaciones de la UGR. Comenzó Ingeniería Informática para aprender a desarrollar videojuegos, pero nunca terminó un juego completo hasta que se presentó a una Game Jam hace unos años.

De los motores que ha probado, Godot Engine es el que mejor conjuga la facildiad de uso, con la curva de aprendizaje, y la facilidad para integrar 2D, 3D y código. Por ello invita a los/as desarrolladores/as a usarlo.

### Contacto(s)

-   Nombre: Juan José Ramos Muñoz
-   Email: <jjramos@ugr.es>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
