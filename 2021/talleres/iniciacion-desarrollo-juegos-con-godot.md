---
layout: 2021/post
category: workshops
author: Andrés Ortiz
title: Iniciación al desarrollo de juegos con Godot
---

## {{ page.author }} - {{ page.title }}

## Descripción

Un taller de iniciación al desarrollo de videojuegos 2D con Godot y gdScript.

## Objetivos a cubrir en el taller

* Qué es Godot y en que nos puede ayudar a desarrollar un videojuego
* Como empezar a desarrollar un juego 2D y conceptos básicos
* Iniciación a programación con gdScript

-   Web del proyecto: <https://godotengine.org/>

## Público objetivo

Gente interesada en desarrollar videojuegos (con y sin experiencia en otros motores o librerías) con un conocimiento básico de programación en cualquier lenguaje.

## Ponente(s)

Tiene experiencia en desarrollo no profesional de videojuegos en multiples motores y librerias.

### Contacto(s)

-   Nombre: Andrés Ortiz
-   Email: <angrykoala@outlook.es>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/the_angry_koala>
-   GitLab: <https://gitlab.com/angrykoala>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/angrykoala>

## Prerrequisitos para los asistentes

Para aquellos que deseen realizar el proyecto del taller, tener Godot 3.3 (<https://godotengine.org/>) instalado.

## Comentarios
